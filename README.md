# Angular2 TodoMVC
This is a sample application of the TodoMVC written using Angular2

## Prerequisites
[Node.JS](https://nodejs.org) v6+, [npm](https://npmjs.org) v3+ and a live internet connection are required to run this application. The internet connection is needed as some resources are pointing to a CDN.

## Commands
Navigate to the root folder and execute the following commands

### Installing the application

```
$ npm install
```

### Running the application

```
$ npm start
```