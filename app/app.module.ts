// angular modules/components
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

// app modules/components
import { AppComponent } from './app.component';
import { TodoContainerComponent } from './todo/todo-container.component';
import { TodoListComponent } from './todo/todo-list.component';
import { TodoService } from './todo/todo.service';
import { TodoMock }  from './todo/todo.mock';
import { TodoFilterPipe } from  './todo/todo-filter.pipe';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(TodoMock),
        RouterModule.forRoot([
            { path: '', redirectTo: 'todo', pathMatch: 'full' },
            { path: 'todo', component: TodoContainerComponent },
            { path: 'todo/:filter', component: TodoContainerComponent }                     
        ])
    ],
    declarations: [
        AppComponent,
        TodoContainerComponent,        
        TodoListComponent,
        TodoFilterPipe
    ],
    providers: [
        TodoService
    ],
    bootstrap:[
        AppComponent
    ]
})

export class AppModule {
}
