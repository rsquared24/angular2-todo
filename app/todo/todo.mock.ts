export class TodoMock {
    createDb(){
        let todos = [
            { id: 1, name: "Go outside", completed: true },
            { id: 2, name: "See the world", completed: false }
        ];
        return {
            todos
        };
    }
}