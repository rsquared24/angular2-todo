import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Todo } from './todo';
import { TodoMock } from './todo.mock';

@Injectable()
export class TodoService {

    private base = "app/todos";
    private headers = new Headers({'Content-Type': 'application/json'});
    private requestHeaders = {headers: this.headers};

    constructor(private http: Http) {

    }

    getAll(): Promise<Todo[]> {
        return this.http
            .get(this.base)
            .toPromise()
            .then((resp) => {
                return resp.json().data as Todo[];
            });  
    }

    delete(id: number): Promise<Todo> {
        return this.http
            .delete(`${this.base}/${id}`, {headers: this.headers})
            .toPromise()
            .then(() => null)
    }

    save(todo: Todo): Promise<Todo> {
        return (!todo.id) ? this.insert(todo) : this.update(todo);
    }

    private insert(todo: Todo): Promise<Todo> {      
        return this.http
            .post(this.base, JSON.stringify(todo), this.requestHeaders)
            .toPromise()
            .then((resp) => {
                return resp.json().data;
            })
    }

    private update(todo: Todo): Promise<Todo> {
        return this.http
            .put(`${this.base}/${todo.id}`, JSON.stringify(todo), this.requestHeaders)
            .toPromise()
            .then(() => {
                return todo;
            });
    }
}