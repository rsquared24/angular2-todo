export class Todo {
    id: number;

    constructor(
        public name: string,
        public completed: boolean) {        
    }
}