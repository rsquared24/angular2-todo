import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Todo } from './todo';
import { TodoService } from './todo.service';

@Component({
    moduleId: module.id,
    selector: 'todo-list',
    templateUrl: 'todo-list.component.html'
})

export class TodoListComponent implements OnInit { 
    // public vars
    filter: string;
    todos: Todo[] = [];
    noOfActive: number = 0;
    noOfCompleted: number = 0;

    // constructor
    constructor(
        private todoService: TodoService,
        private route: ActivatedRoute) {
    }

    // private methods
    private completedTodos(): Todo[] {
        return this.todos.filter(x => x.completed);
    }

    private activeTodos(): Todo[] {
        return this.todos.filter(x => !x.completed);
    }

    private calculate(): void {
        this.noOfActive = this.activeTodos().length;
        this.noOfCompleted = this.completedTodos().length;
    }

    // implemented interfaces
    ngOnInit(): void {
        this.route.params.forEach((params: Params) => {
            this.filter = params['filter'] || 'all';        
        })

        this.getAll();
    }

    // public methods   
    addTodo(name: string): void {
        if(!name) {
            return;
        }

        let todo = new Todo(name, false);
        
        this.todoService.save(todo)
            .then((resp) => {
                this.todos.push(resp);
                this.calculate();                        
            });
    }

    updateTodo(todo: Todo): void {
        this.todoService.save(todo)
            .then((resp) => {
                todo = resp;
                this.calculate();
            });
    }

    removeCompleted(): void{
        let completed = this.completedTodos();
        for(let i = 0; i < completed.length; i++) {
            let todo = completed[i];
            this.removeTodo(todo);
        }
    }

    removeTodo(todo: Todo): void {
        this.todoService.delete(todo.id)
            .then((resp) => {
                this.todos = this.todos.filter(x => x !== todo);
                this.calculate();
            });
    }

    getAll(): void {
        this.todoService.getAll()
            .then((resp) => {
                this.todos = resp;
                this.calculate();               
            }); 
    }

    checkAll(): void {
        if(this.todos.length <= 0) {
            return;
        }

        let checked = (this.noOfCompleted >= 0) && (this.noOfCompleted != this.todos.length);

        for(let i = 0; i < this.todos.length; i++){
            let todo = this.todos[i];
            todo.completed = checked;
        }

        this.calculate();
    }
}