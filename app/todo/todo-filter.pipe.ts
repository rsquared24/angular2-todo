import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from './todo';

@Pipe({name:'todoFilter', pure: false})
export class TodoFilterPipe implements PipeTransform {
    transform(todos: Todo[], filter: string) {
        if(!todos) {
            return;
        }

        let converted = this.convert(filter);

        if(converted === null) {
           return todos; 
        }

        return todos.filter((todo) => {
            return todo.completed == converted;            
        });
    }

    convert(value: string): boolean {
        let formatted = value.toLowerCase().trim();
        let rtn: boolean;

        switch(formatted) {
            case 'active':
                rtn = false;
                break;
            case 'completed':
                rtn = true;
                break;
            default:
                rtn = null;
                break;                            
        }

        return rtn;
    }
}