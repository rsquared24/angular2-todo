import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app',
    styleUrls: ['app.css'],
    templateUrl: 'app.component.html'
})

export class AppComponent {
}